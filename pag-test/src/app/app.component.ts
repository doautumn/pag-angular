import { AfterViewInit, Component } from '@angular/core';
import { PAGInit } from 'libpag';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    PAGInit().then((PAG) => {
      const url = './assets/soar(1).pag';
      fetch(url)
        .then((response) => response.blob())
        .then(async (blob) => {
          const file = new window.File([blob], url.replace(/(.*\/)*([^.]+)/i, '$2'));
          const pagFile = await PAG.PAGFile.load(file);
          const dom: any = document.getElementById('pag');
          dom.width = pagFile.width();
          dom.height = pagFile.height();
          const pagView: any = await PAG.PAGView.init(pagFile, '#pag');
          pagView.setRepeatCount(0);
          await pagView.play();
        });
    });
  }
}
