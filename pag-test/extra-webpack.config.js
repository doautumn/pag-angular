const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require("path");

module.exports = {
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './node_modules/libpag/lib/libpag.wasm'),
          to: path.resolve(__dirname, './dist/pag-test')
        },
      ]
    })
  ]
};