# Angular使用PAG格式图片

[PAG官网](https://pag.io/)

### 1、安装依赖

```
npm i libpag
npm i @angular-builders/custom-webpack @types/emscripten @types/offscreencanvas copy-webpack-plugin path -D
```

### 2、添加自定义webpack配置文件

根目录下创建extra-webpack.config.js

```
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './node_modules/libpag/lib/libpag.wasm'),
          to: path.resolve(__dirname, './dist/<project-name>')   // <project-name>替换为自己的工程名
        },
      ]
    })
  ]
};
```

### 3、修改angular.json

```
"architect": {
  ...
  "build": {
    "builder": "@angular-builders/custom-webpack:browser",
    "options": {
      "customWebpackConfig": {
         "path": "./extra-webpack.config.js"
      },
      ...
    }
  },
  "serve": {
    "builder": "@angular-builders/custom-webpack:dev-server",
    "options": {
      "browserTarget": "<project-name>:build"   // <project-name>替换为自己的工程名
    }
  }
```

### 4、编写代码

xxx.component.html

```
<canvas class="canvas" id="pag"></canvas>
```

xxx.component.ts

```
ngAfterViewInit(): void {
    PAGInit().then((PAG) => {
      const url = './assets/xxx.pag';
      fetch(url)
        .then((response) => response.blob())
        .then(async (blob) => {
          const file = new window.File([blob], url.replace(/(.*\/)*([^.]+)/i, '$2'));
          const pagFile = await PAG.PAGFile.load(file);
          const dom: any = document.getElementById('pag');
          dom.width = pagFile.width();
          dom.height = pagFile.height();
          const pagView: any = await PAG.PAGView.init(pagFile, '#pag');
          pagView.setRepeatCount(0);
          await pagView.play();
        });
    });
  }
```

